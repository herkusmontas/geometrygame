﻿using System;

namespace GeometryGame.Core
{
    /// <summary>
    /// The class contains game settings.
    /// </summary>
    public class GameSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameSettings"/> class.
        /// </summary>
        /// <param name="height">The height of the game field. It must be not less than 20 and not bigger than 40.</param>
        /// <param name="width">The width of the game field. It must be not less than 30 and not bigger than 50.</param>
        /// <param name="movesNumber">The number of moves for each player. It must be 20 at least.</param>
        /// <exception cref="ArgumentException">Thrown when some of the above parameters are out of their bounds.</exception>
        public GameSettings(int height = 20, int width = 30, int movesNumber = 20)
        {
            if (height < 20 || height > 40)
            {
                throw new ArgumentException("Field height (rows number) must be 20-40.", nameof(height));
            }

            if (width < 30 || width > 50)
            {
                throw new ArgumentException("Field width (columns number) must be 30-50.", nameof(width));
            }

            if (movesNumber < 20)
            {
                throw new ArgumentException("Number of moves must be 20 at least.", nameof(movesNumber));
            }

            this.Field = new int[height, width];
            this.MovesNumber = movesNumber;
        }

        public char PlayerOneChar { get; set; } = '#';

        public char PlayerTwoChar { get; set; } = '@';

        public char FieldChar { get; set; } = '-';

        public int[,] Field { get; }

        public int Height
        {
            get
            {
                return this.Field.GetLength(0);
            }
        }

        public int MovesNumber { get; }

        public int Width
        {
            get
            {
                return this.Field.GetLength(1);
            }
        }
    }
}