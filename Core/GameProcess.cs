﻿using System;
using System.Threading;

namespace GeometryGame.Core
{
    /// <summary>
    /// The class contains methods to handle the game process, public method to start the game.
    /// </summary>
    public class GameProcess
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameProcess"/> class.
        /// </summary>
        /// <param name="settings">Game settings instance.</param>
        public GameProcess(GameSettings settings = null)
        {
            this.Settings = settings ?? new GameSettings();
        }

        public GameSettings Settings { get; set; }

        /// <summary>
        /// Starts game process.
        /// </summary>
        public void Start()
        {
            Player currentPlayer = 0;
            int
                movesLeft = this.Settings.MovesNumber * 2,
                firstCubeResult,
                secondCubeResult,
                playerOneScore,
                playerTwoScore,
                xPos,
                yPos;

            while (movesLeft != 0)
            {
                // Switch player.
                if (currentPlayer == Player.One)
                {
                    currentPlayer = Player.Two;
                }
                else
                {
                    currentPlayer = Player.One;
                }

                // Roll figure sizes.
                RedrawField();
                Console.WriteLine("P1: {0}\nP2: {1}", this.GetScore(Player.One), this.GetScore(Player.Two));
                Console.WriteLine("Moves left: {0}", movesLeft);
                Console.WriteLine("Player " + (int)currentPlayer);
                firstCubeResult = GetCubeNumber();
                secondCubeResult = GetCubeNumber();
                Console.WriteLine("Rolled. Width and height of your figure: {0} x {1}", firstCubeResult, secondCubeResult);
                if (!this.IsPossible(firstCubeResult, secondCubeResult))
                {
                    firstCubeResult = GetCubeNumber();
                    secondCubeResult = GetCubeNumber();
                    Console.WriteLine("No place for such figure. You have an extra try.");
                    Console.WriteLine("Rolled again. Width and height of your figure: {0} x {1}", firstCubeResult, secondCubeResult);
                    if (!this.IsPossible(firstCubeResult, secondCubeResult))
                    {
                        Console.WriteLine("No place for such figure. Press any key to end the game.");
                        Console.ReadKey();
                        break;
                    }
                }

                // Enter coordinates to place the figure.
                Console.WriteLine("Enter coordinates to place the figure.");
                do
                {
                    EnterAndCheck(out xPos, 1, this.Settings.Width, "X: ");
                    EnterAndCheck(out yPos, 1, this.Settings.Height, "Y: ");

                    if (this.OutOfBounds(xPos, yPos, firstCubeResult, secondCubeResult))
                    {
                        Console.WriteLine("Figure cannot go out of field bounds.");
                        continue;
                    }

                    if (!this.PlaceIsFree(xPos, yPos, firstCubeResult, secondCubeResult))
                    {
                        Console.WriteLine("Figure cannot step on other figures.");
                        continue;
                    }

                    break;
                }
                while (true);

                this.AddFigure(firstCubeResult, secondCubeResult, xPos, yPos, currentPlayer);

                movesLeft--;
            }

            // Game results.
            RedrawField();
            playerOneScore = this.GetScore(Player.One);
            playerTwoScore = this.GetScore(Player.Two);
            Console.WriteLine("P1: {0}\nP2: {1}", playerOneScore, playerTwoScore);
            if (playerOneScore > playerTwoScore)
            {
                Console.WriteLine("Player 1 wins!");
            }
            else if (playerOneScore == playerTwoScore)
            {
                Console.WriteLine("Draw!");
            }
            else
            {
                Console.WriteLine("Player 2 wins!");
            }

            int GetCubeNumber()
            {
                Thread.Sleep(1);
                Random rand = new Random();
                return rand.Next(1, 7);
            }

            void RedrawField()
            {
                Console.Clear();
                this.DrawField();
            }
        }

        public void DrawField()
        {
            Console.Write($"{string.Empty,3}");
            for (int i = 1; i <= this.Settings.Width; i++)
            {
                Console.Write($"{i,3}");
            }

            Console.WriteLine();

            for (int i = 0; i < this.Settings.Height; i++)
            {
                Console.Write($"{i + 1,3}");
                for (int j = 0; j < this.Settings.Width; j++)
                {
                    switch (this.Settings.Field[i, j])
                    {
                        case 1:
                            {
                                Console.Write($"{this.Settings.PlayerOneChar,3}");
                                break;
                            }

                        case 2:
                            {
                                Console.Write($"{this.Settings.PlayerTwoChar,3}");
                                break;
                            }

                        default:
                            {
                                Console.Write($"{this.Settings.FieldChar,3}");
                                break;
                            }
                    }
                }

                Console.WriteLine();
            }
        }

        /// <summary>
        /// Counts a player's score (a count of his symbols on the field).
        /// </summary>
        /// <param name="player">A player whose score to count.</param>
        /// <returns>Player's score.</returns>
        public int GetScore(Player player)
        {
            int score = 0;
            foreach (int i in this.Settings.Field)
            {
                if (i == (int)player)
                {
                    score++;
                }
            }

            return score;
        }

        /// <summary>
        /// A universal method to handle a variable's init.
        /// </summary>
        /// <param name="variable">The variable.</param>
        /// <param name="min">Minimum value.</param>
        /// <param name="max">Maximum value.</param>
        /// <param name="message">A message to print before read.</param>
        /// <exception cref="ArgumentException">Thrown when the minimum value is bigger than the maximum value.</exception>
        public static void EnterAndCheck(out int variable, int min, int max, string message)
        {
            if (min > max)
			{
                throw new ArgumentException("Minimum value cannot be bigger than maximum value.");
			}

            do
            {
                Console.Write(message);
                if (!int.TryParse(Console.ReadLine(), out variable))
                {
                    Console.WriteLine("Error. Not a number.");
                    continue;
                }

                if (variable < min || variable > max)
                {
                    Console.WriteLine("Error. Number must be not less than {0} and not bigger than {1}.", min, max);
                    continue;
                }

                break;
            }
            while (true);
        }

        /// <summary>
        /// Draws a figure with player's symbols. If the figure is out of field bounds or the choosen place isn't free, it won't be drawn.
        /// </summary>
        /// <param name="w">The width of the figure.</param>
        /// <param name="h">The height of the figure.</param>
        /// <param name="x">Column number.</param>
        /// <param name="y">Row number.</param>
        /// <param name="player">The player who draws.</param>
        /// <exception cref="ArgumentException">Thrown when the above parameters are out of their bounds.</exception>
        private void AddFigure(int w, int h, int x, int y, Player player)
        {
            if (x < 1 || x > this.Settings.Width)
            {
                throw new ArgumentException("X position cannot be less than 1 or bigger than the field's width.", nameof(x));
            }

            if (y < 1 || y > this.Settings.Height)
            {
                throw new ArgumentException("Y position cannot be less than 1 or bigger than the field's height.", nameof(y));
            }

            if (w < 1 || h < 1)
            {
                throw new ArgumentException("Figure's width or height cannot be less than 1.");
            }

            if ((int)player != 1 && (int)player != 2)
            {
                throw new ArgumentException("There are only player 1 and player 2.", nameof(player));
            }

            if (this.OutOfBounds(x, y, w, h) ||
                !this.PlaceIsFree(x, y, w, h))
            {
                return;
            }

            for (int i = y - 1; i < y - 1 + h; i++)
            {
                for (int j = x - 1; j < x - 1 + w; j++)
                {
                    this.Settings.Field[i, j] = (int)player;
                }
            }
        }

        /// <summary>
        /// Checks if it is a place anywhere on the field where a figure of certain width and height can be placed.
        /// </summary>
        /// <param name="w">The width of the figure.</param>
        /// <param name="h">The height of the figure.</param>
        /// <returns>True if it's possible to place the figure somewhere of the field.</returns>
        /// <exception cref="ArgumentException">Thrown when the above parameters are out of their bounds.</exception>
        private bool IsPossible(int w, int h)
        {
            if (w < 1 || h < 1)
            {
                throw new ArgumentException("Figure's width or height cannot be less than 1.");
            }

            for (int i = 0; i < this.Settings.Height; i++)
            {
                for (int j = 0; j < this.Settings.Width; j++)
                {
                    if (this.Settings.Field[i, j] == 0)
                    {
                        if (!this.OutOfBounds(j + 1, i + 1, w, h))
                        {
                            if (this.PlaceIsFree(j + 1, i + 1, w, h))
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if a figure of certain width and height violates the field's bounds while being placed in certain position.
        /// </summary>
        /// <param name="x">X position of the figure (column number).</param>
        /// <param name="y">Y position of the figure (row number).</param>
        /// <param name="w">The width of the figure.</param>
        /// <param name="h">The height of the figure.</param>
        /// <returns>True if the figure violates the field's bounds.</returns>
        /// <exception cref="ArgumentException">Thrown when the above parameters are out of their bounds.</exception>
        private bool OutOfBounds(int x, int y, int w, int h)
        {
            if (x < 1 || x > this.Settings.Width)
            {
                throw new ArgumentException("X position cannot be less than 1 or bigger than the field's width.", nameof(x));
            }

            if (y < 1 || y > this.Settings.Height)
            {
                throw new ArgumentException("Y position cannot be less than 1 or bigger than the field's height.", nameof(y));
            }

            if (w < 1 || h < 1)
            {
                throw new ArgumentException("Figure's width or height cannot be less than 1.");
            }

            return x - 1 + w > this.Settings.Width ||
                y - 1 + h > this.Settings.Height;
        }

        /// <summary>
        /// Checks if the certain area if free to place a figure.
        /// </summary>
        /// <param name="x">X position of the figure (column number).</param>
        /// <param name="y">Y position of the figure (row number).</param>
        /// <param name="w">The width of the figure.</param>
        /// <param name="h">The height of the figure.</param>
        /// <returns>True if the chosen area is free to place a figure of certain width and height.</returns>
        /// <exception cref="ArgumentException">Thrown when the above parameters are out of their bounds.</exception>
        private bool PlaceIsFree(int x, int y, int w, int h)
        {
            if (x < 1 || x > this.Settings.Width)
            {
                throw new ArgumentException("X position cannot be less than 1 or bigger than the field's width.", nameof(x));
            }

            if (y < 1 || y > this.Settings.Height)
            {
                throw new ArgumentException("Y position cannot be less than 1 or bigger than the field's height.", nameof(y));
            }

            if (w < 1 || h < 1)
            {
                throw new ArgumentException("Figure's width or height cannot be less than 1.");
            }

            for (int i = y - 1; i < y - 1 + h; i++)
            {
                if (i >= this.Settings.Height)
                {
                    break;
                }

                for (int j = x - 1; j < x - 1 + w; j++)
                {
                    if (j >= this.Settings.Width)
                    {
                        break;
                    }

                    if (this.Settings.Field[i, j] != 0)
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    }
}