﻿namespace GeometryGame.Core
{
    public enum Player
    {
        /// <summary>
        /// Player one.
        /// </summary>
        One = 1,

        /// <summary>
        /// Player two.
        /// </summary>
        Two = 2,
    }
}
