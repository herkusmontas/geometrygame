﻿using GeometryGame.Core;
using System;

namespace GeometryGame
{
	class Program
	{
		static void Main(string[] args)
		{
			int
				height,
				width,
				movesNumber;
			GameProcess game;

			Console.Title = "Geometry Game";
			Console.WriteLine("Press any key to start the game.");
			Console.ReadKey();
			Console.WriteLine("Do you want to set the game settings? (Y/N)");
			
			if (Console.ReadKey().Key == ConsoleKey.Y)
			{
				GameProcess.EnterAndCheck(out height, 20, 40, "Field height (20-40): ");
				GameProcess.EnterAndCheck(out width, 30, 50, "Field width (30-50): ");
				GameProcess.EnterAndCheck(out movesNumber, 20, 100, "Moves number (for each player): (20-100): ");
				game = new GameProcess(new GameSettings(height, width, movesNumber));
			}
			else
			{
				game = new GameProcess();
			}

			game.Start();
			
			Console.ReadLine();
		}
	}
}
